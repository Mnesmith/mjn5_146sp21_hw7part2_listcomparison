/*
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
*/

import java.util.*; // wildcard import of all classes and interfaces in java.util

/**
 * Application class to demonstrate the performance of an ArrayList and
 * that of a LinkedList appear to be congruent with theory
 * @author mjn5@email.sc.edu
 * @version 146sp21_hw7part2
 */
public class ListComparisonDemoApp {
    
    /**
     * Main method for the app
     * @param args
     */
    public static void main( String[] args )
    {
        String[] myStrings = { "zero", "one", "three", "four" };
        
        System.out.println("Original array of strings: " +
                Arrays.toString( myStrings ) + "\n" );
        
        // Create two List objects (one ArrayList, one LinkedList) that
        // both contain the same data
        List<String> myArrayList = new ArrayList<>( Arrays.asList( myStrings ) );
        List<String> myLinkedList = new LinkedList<>( Arrays.asList( myStrings ) );
        
        // Use System.nanoTime() to help compute the elapsed time
        // it takes to execute a section of code
        
        long startTime = System.nanoTime();
        myArrayList.add( 2, "two" );
        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert element in the middle" +
                " of a ArrayList: " + elapsedTime);
        
        startTime = System.nanoTime();
        myLinkedList.add( 2, "two" );
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert element in the middle" +
                " of a LinkedList: " + elapsedTime);
        
        System.out.println( myArrayList );
        System.out.println( myLinkedList );
        
        String middleElement = "";
        
        startTime = System.nanoTime();
        middleElement = myArrayList.get( 2 ); // retrienves element at index 2
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to access element in the middle" +
                " of myArrayList: " + elapsedTime);
        
        //System.out.println("Middle element of myArrayList: " + middleElement);
        
        startTime = System.nanoTime();
        middleElement = myLinkedList.get( 2 ); // retrienves element at index 2
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to access element in the middle" +
                " of myLinkedList: " + elapsedTime);
        
        //System.out.println("Middle element of myLinkedList: " + middleElement);
        
        /*
        In summary:
        
        ArrayLists and LinkedLists are LOGICALLY similar,
        but their RUN-TIME PERFORMANCE is different.
        
        ArrayLists:
        Faster ( constant time, O(c) ) for random access
        Slower ( linear time, O(n) ) for insertion/removals
        
        LinkedList:
        Slower ( linear time, O(n) for random access (looking up an element by its index)
        Faster ( constant time, O(c) ) for insertion/removals
        ( strictly speaking, constant time applies when inserting/removing
        from the head or tail, since no additional steps are required
        for looking up the element at some index in the middle, which
        does take linear time or has O(n) complexity )
        */
    } // end method main
    
} // end class ListComparisonDemoApp
